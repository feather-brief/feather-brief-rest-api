const express = require("express");
const { Readability } = require("@mozilla/readability");
const JSDOM = require("jsdom").JSDOM;
const rssFinder = require("rss-finder");
const app = express();
const port = 3000;

app.get("/readability", (req, res) => {
  const url = req.query.url;
  if (!url) {
    res.send({});
  } else {
    JSDOM.fromURL(url).then((dom) => {
      const locationMetadata = dom.window.location;
      const uri = {
        spec: locationMetadata.href,
        host: locationMetadata.host,
        prePath: `${locationMetadata.protocol}//${locationMetadata.host}`,
        scheme: locationMetadata.protocol.substr(
          0,
          locationMetadata.protocol.indexOf(":")
        ),
        pathBase: `${locationMetadata.protocol}//${
          locationMetadata.host
        }${locationMetadata.pathname.substr(
          0,
          locationMetadata.pathname.lastIndexOf("/") + 1
        )}`,
      };

      const html = dom.serialize();
      const { document } = new JSDOM(html).window;

      const article = new Readability(uri, document).parse();
      res.send(article);
    });
  }
});

app.get("/feed", (req, res) => {
  const url = req.query.url;
  console.log(url);
  rssFinder(url)
    .then(function (result) {
      res.send(result);
    })
    .catch(function (err) {
      res.send(err);
    });
});

app.listen(port, () => {
  console.log(`Running readability server on http://localhost:${port}`);
});
