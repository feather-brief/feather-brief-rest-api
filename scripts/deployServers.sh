export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
nvm use 11.15.0
cd server

# Deploying main server
pm2 stop main-server
pm2 delete main-server
mkdir file-uploads
mkdir favicons
pm2 start launchServer.sh -n main-server

# Deploying readability server
rm -R readability-server
tar -xvf readability-server.tar
cd readability-server
pm2 stop readability-server
pm2 delete readability-server
npm install
pm2 start index.js -n readability-server
cd ..

cd ..
