grep "url\" :" feeds.txt | cut -d\" -f4 > urls
cat urls | cut -d/ -f3 > filename
paste -d\; urls filename > filenameAndUrls

OLDIFS=$IFS
IFS=";"

cd favicons

username=
password=

while read url name; 
do
    favicon="$name.ico"
    QUERY="{\"query\": {\"bool\": {\"must\": [{\"match\": {\"url.keyword\": \"$url\"}}]}}, \"script\": {\"source\": \"ctx._source.favicon='$favicon'\",\"lang\": \"painless\"}}"
    wget "icons.duckduckgo.com/ip3/$name.ico";
    curl -XPOST "http://$username:$password@$serverUrl:9200/feed/_update_by_query" -H 'Content-Type: application/json' -d $QUERY
    sleep 1
done < ../filenameAndUrls

IFS=$OLDIFS
cd ..
rm filename
rm urls
rm filenameAndUrls