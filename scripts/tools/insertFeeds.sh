#!/bin/bash
# Purpose: Read Comma Separated CSV File
# Author: Vivek Gite under GPL v2.0+
# ------------------------------------------
INPUT=top-feeds-en.csv
OLDIFS=$IFS
IFS=','
[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }
while read url feedname
do
	echo "url : $url"
	echo "feed : $feedname"
    payload="{ \"url\" : \"$url\", \"name\" : \"$feedname\" }"
    curl --request POST --url http://localhost:8080/secured/feeds -d "$payload"
done < $INPUT
IFS=$OLDIFS
