package io.telary.featherbrief.model

case class Args(
                 configFile: String,
                 pubKeyFile: String,
                 isListenerRun: Boolean,
                 isServerRun: Boolean,
                 isDebugMode: Boolean,
                 debugEmail: String
               ) {

}
