package io.telary.featherbrief.model.objects

object Indices {
  private val TEST_PREFIX = "test_"
  val FEED: String = "feed"
  val FEED_TEST: String = TEST_PREFIX + FEED
  val USER_FEED_ITEM: String = "userfeeditem"
  val USER_FEED_ITEM_TEST: String = TEST_PREFIX + USER_FEED_ITEM
  val USER_FEED: String = "feeduser"
  val USER_FEED_TEST: String = TEST_PREFIX + USER_FEED
  val ARTICLE: String = "article"
  val ARTICLE_TEST: String = TEST_PREFIX + ARTICLE
  val ACCESS_LOG: String = "accesslog"
  val ACCESS_LOG_TEST: String = TEST_PREFIX + ACCESS_LOG
  val BUNDLE: String = "bundle"
  val BUNDLE_TEST: String = TEST_PREFIX + BUNDLE
}
