package io.telary.featherbrief.model.objects

case class Bundle(
            name : String,
            language : String,
            feeds : List[Feed]
            )
object BundleField {
  val NAME : String = "name"
  val LANGUAGE : String = "language"
  val FEEDS : String = "feeds"
}