package io.telary.featherbrief.model.objects

case class Flag(
                 contentScore : Int,
                 weight : Int
               )
object Flag {
  val STRIP_UNLIKELYS: Int = 0x1
  val WEIGHT_CLASSES: Int = 0x2
  val CLEAN_CONDITIONALLY: Int = 0x4
}
