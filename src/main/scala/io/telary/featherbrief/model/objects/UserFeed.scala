package io.telary.featherbrief.model.objects

import io.telary.featherbrief.model.Defaults

case class UserFeed(
                     feedName : String,
                     feedUrl : String,
                     email : String,
                     category : String = Defaults.DEFAULT_CATEGORY
                   )
object UserFeedField {
  val FEED_NAME = "feedName"
  val FEED_URL = "feedUrl"
  val EMAIL = "email"
  val CATEGORY = "category"
}