package io.telary.featherbrief.model.objects

import io.telary.featherbrief.model.Defaults

case class UserFeedItem(
                         email : String,
                         feedName : String,
                         feedUrl : String,
                         articleUrl : String,
                         author : String,
                         description : String,
                         date : Long,
                         summary : String,
                         title : String,
                         score : Long,
                         shown : Boolean,
                         read : Boolean,
                         saved : Boolean,
                         upvote : Boolean,
                         downvote : Boolean,
                         favicon : String,
                         parsable : Boolean,
                         category: String = Defaults.DEFAULT_CATEGORY
                       )
object UserFeedItemField {
  val EMAIL = "email"
  val FEED_NAME = "feedName"
  val FEED_URL = "feedUrl"
  val ARTICLE_URL = "articleUrl"
  val AUTHOR = "author"
  val DESCRIPTION = "description"
  val DATE = "date"
  val SUMMARY = "summary"
  val TITLE = "title"
  val SCORE = "score"
  val SHOWN = "shown"
  val READ = "read"
  val SAVED = "saved"
  val UPVOTE = "upvote"
  val DOWNVOTE = "downvote"
  val FAVICON = "favicon"
  val PARSABLE = "parsable"
  val CATEGORY = "category"
}
