package io.telary.featherbrief.model.repositories

import java.util.Calendar

import io.telary.featherbrief.model.objects.{Article, ArticleField}
import io.vertx.core.json.JsonObject
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.action.search.{SearchRequest, SearchResponse}
import org.elasticsearch.client.{RequestOptions, RestHighLevelClient}
import org.elasticsearch.common.xcontent.XContentType
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.SearchHit
import org.elasticsearch.search.builder.SearchSourceBuilder

class ArticleRepository(index: String) {

  def save(esClient : RestHighLevelClient, articleAsJson : JsonObject): Unit = {
    val day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
    val month = Calendar.getInstance().get(Calendar.MONTH)
    val year = Calendar.getInstance().get(Calendar.YEAR)
    val indexRequest = new IndexRequest(index + "-" + year + "-" + month + "-" + day)
    indexRequest.source(articleAsJson.encodePrettily(), XContentType.JSON)
    esClient.index(indexRequest, RequestOptions.DEFAULT)
  }

  def get(esClient : RestHighLevelClient, articleUrl : String) : Option[Article] = {
    val request = new SearchRequest(index + "-*")
    val searchSourceBuilder : SearchSourceBuilder = new SearchSourceBuilder()
    searchSourceBuilder.query(QueryBuilders.termQuery(ArticleField.URL + ".keyword", articleUrl))
    searchSourceBuilder.size(1)
    request.source(searchSourceBuilder)
    val searchResponse : SearchResponse = esClient.search(request, RequestOptions.DEFAULT)
    val hits = searchResponse.getHits.getHits
    if (hits.nonEmpty) {
      val hit : SearchHit = hits(0)
      Some(parse(hit.getSourceAsMap))
    } else {
      None
    }
  }

  def parse(map: java.util.Map[String, AnyRef]) : Article = {
    val url = getField(map, ArticleField.URL, "")
    val title = getField(map, ArticleField.TITLE, "")
    val byline = getField(map, ArticleField.BYLINE, "")
    val dir = getField(map, ArticleField.DIR, "")
    val content = getField(map, ArticleField.CONTENT, "")
    val textContent = getField(map, ArticleField.TEXT_CONTENT, "")
    val lengthVal = getField(map, ArticleField.LENGTH, "0")
    val length : Int = lengthVal.toInt
    val excerpt = getField(map, ArticleField.EXCERPT, "")
    val siteName = getField(map, ArticleField.SITE_NAME, "")
    Article(url, title, byline, dir, content, textContent, length, excerpt, siteName)
  }

  private def getField(map : java.util.Map[String, AnyRef], key : String, defaultValue : String) : String = {
    if (map.getOrDefault(key, defaultValue) == null) {
      defaultValue
    } else {
      map.getOrDefault(key, defaultValue).toString
    }
  }
}
