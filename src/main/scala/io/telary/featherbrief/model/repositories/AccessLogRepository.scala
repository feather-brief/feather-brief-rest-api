package io.telary.featherbrief.model.repositories

import java.time.{LocalDateTime, ZoneOffset}
import java.util.Calendar

import io.vertx.core.json.JsonObject
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.client.{RequestOptions, RestHighLevelClient}
import org.elasticsearch.common.xcontent.XContentType

class AccessLogRepository(index : String) {

  def save(esClient : RestHighLevelClient, email : String): Unit = {
    val indexRequest = new IndexRequest(index)
    val dayInt = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
    val day = if(dayInt < 10) { "0" + dayInt } else dayInt
    val monthInt = Calendar.getInstance().get(Calendar.MONTH) + 1
    val month = if(monthInt < 10) { "0" + monthInt } else monthInt
    val year = Calendar.getInstance().get(Calendar.YEAR)
    val json = new JsonObject()
    json.put("email", email)
    json.put("day", "" + year + month + day)
    json.put("date", LocalDateTime.now().toInstant(ZoneOffset.UTC).toEpochMilli)
    indexRequest.source(json.encodePrettily(), XContentType.JSON)
    esClient.index(indexRequest, RequestOptions.DEFAULT)
  }

}
