package io.telary.featherbrief.listener

import java.net.URI
import java.util.Calendar

import com.rometools.rome.feed.synd.{SyndEntry, SyndFeed}
import com.rometools.rome.io.{SyndFeedInput, XmlReader}
import io.telary.featherbrief.controller.utils.ListenerLog
import io.telary.featherbrief.listener.actions.FeedActions
import io.telary.featherbrief.model.objects.{Feed, Indices, UserFeedItem}
import io.telary.featherbrief.model.repositories.{FeedRepository, UserFeedItemRepository, UserFeedRepository}
import io.telary.featherbrief.service.{ConfigFile, FeedParser}
import org.apache.http.HttpHost
import org.apache.http.auth.{AuthScope, UsernamePasswordCredentials}
import org.apache.http.impl.client.BasicCredentialsProvider
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder
import org.elasticsearch.action.admin.cluster.health.ClusterHealthRequest
import org.elasticsearch.client.{RequestOptions, RestClient, RestHighLevelClient}
import org.elasticsearch.cluster.health.ClusterHealthStatus

import scala.collection.JavaConverters._

object Listener extends FeedActions {
  def run(configFilePath: String): Unit = {
    val config = ConfigFile.parse(configFilePath)

    val credentialsProvider = new BasicCredentialsProvider
    credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(config.esUsername, config.esPassword))

    val builder = RestClient.builder(new HttpHost(config.esHost, config.esPort.toInt))
      .setHttpClientConfigCallback((httpClientBuilder: HttpAsyncClientBuilder) => {
        httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider)
      })

    val esClient : RestHighLevelClient = new RestHighLevelClient(builder);

    try {
      val response = esClient.cluster().health(new ClusterHealthRequest(Indices.FEED), RequestOptions.DEFAULT)
      if (response.getStatus != ClusterHealthStatus.GREEN && response.getStatus != ClusterHealthStatus.YELLOW) {
        System.exit(1)
      }
    } catch {
      case e : Exception => {
        new ListenerLog("Establishing connection", "Can't reach ES\n" + e.getMessage ).log()
        System.exit(1)
      }
    }
    /**
     * - Trigger the cron
     * - Get all the feeds we follow for the frequency X
     * - For each feeditem
     *    - Read it
     *    - Store it in ES
     *    - Mark insertion date
     **/
    val now = Calendar.getInstance()
    val frequency = determineFrequencyForRun(now)
    val feedRepository = new FeedRepository(Indices.FEED)
    val followedFeeds : List[Feed] = feedRepository.getForFrequency(esClient, frequency)
    /**
     * Add max_update_date in each Feed
     */
    val feedItemsToInsert : List[UserFeedItem] = followedFeeds.flatMap { feed =>

      /**
       * For each feed -> fetch the .xml file
       */
      try {
        val feedItemList: SyndFeed = FeedParser.get(new URI(feed.url))
        val allFeedItems = feedItemList.getEntries
        var maxPubDate: Long = 0
        /**
         * Parse the .xml file (Sync parsing)
         */
        val newFeedItems = allFeedItems.asScala.filter { entry: SyndEntry =>
          val pubDate = FeedParser.getPubDate(entry)
          maxPubDate = if (pubDate.isDefined && maxPubDate < pubDate.get) pubDate.get else maxPubDate
          pubDate.isDefined && pubDate.get > feed.lastPublicationDate
        }
        /**
         * Si le FeedItem a une date de publication > à max_update_date
         *    - Récupérons l'ensemble des emails qui attendent ce FeedItem
         *    - Insérons dans l'index userfeeditem
         * Si le 1er FeedItem à une date de publication > à max_update_date && qu'elle est set
         *    - On baisse la fréquence et on met à jour le feed
         * Lors du dernier FeedItem, on met à jour max_update_date avec sa date de publication
         */
        if (newFeedItems.nonEmpty) {
          val pubFrequency = if (feed.lastPublicationDate > 0
            && feed.publicationFrequency < 6
          ) {
            if (newFeedItems.size == allFeedItems.size()) {
              feed.publicationFrequency + 1
            } else {
              feed.publicationFrequency
            }
          } else {
            feed.publicationFrequency
          }
          feedRepository.update(esClient, feed.url, pubFrequency, maxPubDate)
          val userFeedRepository = new UserFeedRepository(Indices.USER_FEED)
          val emails: List[(String, String)] = userFeedRepository.getAllUserFollowing(esClient, feed.url)
          new ListenerLog("Treating the feed %s".format(feed.name)).log
          newFeedItems.flatMap { entry: SyndEntry =>
            emails.map { email: (String,String) =>
              UserFeedItem(
                email._1,
                feed.name,
                feed.url,
                entry.getLink,
                entry.getAuthor,
                "",
                entry.getPublishedDate.toInstant.toEpochMilli,
                "",
                entry.getTitle,
                score = ((1.toFloat / feed.publicationFrequency) * entry.getPublishedDate.toInstant.toEpochMilli).toLong,
                shown = false,
                read = false,
                saved = false,
                upvote = false,
                downvote = false,
                feed.favicon,
                feed.parsable,
                email._2
              )
            }
          }
        } else {
          List()
        }
      } catch {
        case e: Exception =>
          new ListenerLog("Feed processing", "An error occurred while processing feed " + feed.name + " :" + e.getMessage).log
          e.printStackTrace()
          List()
      }
    }.toList

    val userFeedItemRepository = new UserFeedItemRepository(Indices.USER_FEED_ITEM)
    userFeedItemRepository.insertAll(esClient, feedItemsToInsert)
    esClient.close()
    new ListenerLog("End of the process").log
  }
}
