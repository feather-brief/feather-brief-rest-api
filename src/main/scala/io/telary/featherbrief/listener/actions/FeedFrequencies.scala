package io.telary.featherbrief.listener.actions

trait FeedFrequencies {
  /* NEVER USE 0 ! */
  val WEEKLY : Int = 1
  val DAILY : Int = 2
  val HOURLY : Int = 3
  val HALF_HOURLY : Int = 4
  val TENTH_MINUTE : Int = 5
  val EVERY_MINUTE : Int = 6
  val INVALID : Int = -1
}
