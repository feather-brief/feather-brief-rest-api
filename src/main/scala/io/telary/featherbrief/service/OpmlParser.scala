package io.telary.featherbrief.service

import io.telary.featherbrief.model.objects.Feed
import org.jdom2.filter.Filters.text

import scala.io.Source
import scala.xml.XML

object OpmlParser {
  def parse(filename : String, email : String) : List[Feed] = {
    val file = Source.fromFile(filename)
    val text = file.getLines.mkString
    file.close()
    val xml = XML.loadString(text)  \ "body" \ "outline"
    val map : Seq[Feed] = xml.flatMap(feeds => {
      feeds.child.map( feed => {
        val feedName = feed.attribute("title")
        val feedUrl = feed.attribute("xmlUrl")
        val language = ""
        val description = ""
        if (feedName.isDefined && feedUrl.isDefined) {
          Some(Feed(feedName.get.text, feedUrl.get.text, -1, 0L, isInStandby = false, language, description, "", email, true))
        } else {
          None
        }
      })
    }).filter(feed => feed.isDefined)
      .map(feed => feed.get)
    map.toList
  }
}
