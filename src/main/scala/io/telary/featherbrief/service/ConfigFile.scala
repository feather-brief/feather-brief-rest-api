package io.telary.featherbrief.service

import io.telary.featherbrief.model.Parameters

object ConfigFile {
  def parse(filePath: String) : Parameters = {
    val file = scala.io.Source.fromFile(filePath)
    val lines = file.getLines.toList
    val configMap = lines.map{ line =>
      val splitLine = line.split('=')
      (splitLine(0), splitLine(1))
    }.toMap
    val param = Parameters(
      configMap.getOrElse("JWT_SECRET",""),
      configMap.getOrElse("ES_USERNAME",""),
      configMap.getOrElse("ES_PASSWORD",""),
      configMap.getOrElse("ES_HOST",""),
      configMap.getOrElse("ES_PORT",""),
      configMap.getOrElse("USER_AGENT",""),
      configMap.getOrElse("SERVER_PORT",""),
      configMap.getOrElse("AUTH0_URL", ""),
      configMap.getOrElse("AUTH0_CLIENT_ID", ""),
      configMap.getOrElse("AUTH0_CLIENT_SECRET", ""),
      configMap.getOrElse("AUTH0_AUDIENCE", ""),
      configMap.getOrElse("AUTH0_GRANT_TYPE", ""),
      configMap.getOrElse("NODE_SERVER_HOST", ""),
      configMap.getOrElse("NODE_SERVER_PORT", ""),
      configMap.getOrElse("SALT", "")
    )
    file.close
    param
  }

  def read(filePath: String) : String = {
    val file = scala.io.Source.fromFile(filePath)
    val lines = file.getLines.toList
    val content = new StringBuilder()
    lines.foreach{ line =>
      content.append(line + "\n")
    }
    content.toString()
  }
}
