package io.telary.featherbrief.service

import java.io.ByteArrayInputStream
import java.net.URI
import java.nio.charset.StandardCharsets

import com.rometools.rome.feed.synd.{SyndEntry, SyndFeed}
import com.rometools.rome.io.{SyndFeedInput, XmlReader}
import scalaj.http.{Http, HttpOptions, HttpResponse}

object FeedParser {

  @throws(classOf[Exception])
  def get(uri : URI) : SyndFeed = {
    val response: HttpResponse[String] = Http(uri.toURL.toString).options(
      HttpOptions.followRedirects(true)).asString
    val result = response.body
    val stream: ByteArrayInputStream = new ByteArrayInputStream(result.getBytes(StandardCharsets.UTF_8));
    new SyndFeedInput().build(new XmlReader(stream))
  }

  def getPubDate(entry : SyndEntry): Option[Long] = {
      if (entry.getPublishedDate != null) {
        Some(entry.getPublishedDate.toInstant.toEpochMilli)
      } else if (entry.getUpdatedDate != null) {
        Some(entry.getUpdatedDate.toInstant.toEpochMilli)
      } else {
        None
      }
  }
}
