package io.telary.featherbrief.service

import io.telary.featherbrief.model.Args

object CliArgs {
  def parse(args: Array[String]) : Args = {
    val help =
      """
        |Usage :
        |-s server run
        |-l listener run (needs to be croned)
        |-c [conf file]
        |-p [public key]
        |-d [debug mode]
        |-e [debug email]""".stripMargin
    if (args.isEmpty) {
      println(help)
      System.exit(0)
    }
    val isServerRun = args.contains("-s")
    val isListenerRun = args.contains("-l")
    val isDebugEnabled = args.contains("-d")
    var confFound = false
    var publicKeyFound = false
    var debugEmailFound = false
    var confFile : String = ""
    var pubKey : String = ""
    var debugEmail : String = ""
    args.foreach { arg =>
      if (arg.equals("-c")) {
        confFound = true
      } else {
        if (confFound) {
          confFile = arg
          confFound = false
        }
      }
      if (arg.equals("-p")) {
        publicKeyFound = true
      } else {
        if (publicKeyFound) {
          pubKey = arg
          publicKeyFound = false
        }
      }
      if (arg.equals("-e")) {
        debugEmailFound = true
      } else {
        if (debugEmailFound) {
          debugEmail = arg
          debugEmailFound = false
        }
      }
    }
    Args(confFile, pubKey, isListenerRun, isServerRun, isDebugEnabled, debugEmail)
  }
}
