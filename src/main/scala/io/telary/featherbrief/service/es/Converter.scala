package io.telary.featherbrief.service.es

import io.telary.featherbrief.model.objects.{Article, Bundle, Feed, FeedItem, UserFeed, UserFeedItem}
import io.vertx.core.json.JsonArray
import io.vertx.lang.scala.json.JsonObject

import scala.collection.mutable

object Converter {
  def toJsonObject(entries : Map[String, Any]) : JsonObject = {
    val jsonObject = new JsonObject
    entries.foreach { case(key, value) =>
      jsonObject.put(key, value match {
        case map: Map[String, Any] =>
          toJsonObject(map)
        case _ =>
          value
      })
    }
    jsonObject
  }

  def toJsonObject(name : String, entries : List[mutable.Map[String, AnyRef]]) : JsonObject = {
    val jsonArray = new JsonArray()
    entries.foreach { elt =>
      jsonArray.add(toJsonObject(elt.toMap))
    }
    val jsonObject = new JsonObject()
    jsonObject.put(name, jsonArray)
  }

  def toJsonObject(obj : Any) : JsonObject = {
    val clazz = obj match {
      case _: Feed => classOf[Feed]
      case _: FeedItem => classOf[FeedItem]
      case _: UserFeedItem => classOf[UserFeedItem]
      case _: UserFeed => classOf[UserFeed]
      case _: Article => classOf[Article]
      case _: Bundle => classOf[Bundle]
    }
    val jsonObject = new JsonObject()
    clazz.getDeclaredFields.foreach { f =>
      f.setAccessible(true)
      f.get(obj) match {
        case list: List[Object]=>
          val array = toJsonArray(list)
          jsonObject.put(f.getName, array)
        case _ =>
          jsonObject.put(f.getName, f.get(obj))
      }
      f.setAccessible(false)
    }
    jsonObject
  }

  def toJsonArray(list : List[Object]) : JsonArray = {
    val array = new JsonArray()
    list.foreach(elt => {
      array.add(toJsonObject(elt))
    })
    array
  }
}
