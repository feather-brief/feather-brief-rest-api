package io.telary.featherbrief

import io.telary.featherbrief.listener.Listener
import io.telary.featherbrief.server.Server
import io.telary.featherbrief.service.CliArgs

object FeatherBrief {
  def main(args: Array[String]): Unit = {
    val cliArgs = CliArgs.parse(args)
    if (cliArgs.isServerRun) {
      Server.run(cliArgs.configFile, cliArgs.pubKeyFile, cliArgs.isDebugMode, cliArgs.debugEmail)
    } else if (cliArgs.isListenerRun) {
      Listener.run(cliArgs.configFile)
    } else {
      println("Error")
    }
  }
}
