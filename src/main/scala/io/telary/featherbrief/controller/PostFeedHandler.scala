package io.telary.featherbrief.controller

import java.net.URI
import java.nio.file.{Files, Paths}
import com.rometools.rome.feed.synd.SyndFeed
import io.telary.featherbrief.controller.utils.{EsClientWrapper, RouteHandler, RouteParams, RouteResponse}
import io.telary.featherbrief.listener.actions.FeedActions
import io.telary.featherbrief.model.Defaults
import io.telary.featherbrief.model.objects.{Indices, UserFeed, UserFeedItemField}
import io.telary.featherbrief.model.repositories.{FeedRepository, UserFeedItemRepository, UserFeedRepository}
import io.telary.featherbrief.service.{FeedParser, NodeClient}
import io.vertx.ext.web.FileUpload
import io.vertx.ext.web.client.WebClient

import scala.collection.JavaConverters._

object PostFeedHandler extends RouteHandler with FeedActions {
  /**
   * Add a new followed feed to an account
   * If the feed doesn't exist yet, add it in the global feed list
   *
   * @param routeParams : path and body params
   * @return the response
   */
  override def process(routeParams: RouteParams,
                                 webClient: Option[WebClient],
                                 esWrapper: Option[EsClientWrapper],
                                 nodeClient: Option[NodeClient],
                                 uploadedFiles: Option[List[FileUpload]]): RouteResponse = {
    val feedUrl = routeParams.body.getOrElse("url", "").toString
    val feedName = routeParams.body.getOrElse("name", "").toString
    val userCategory = routeParams.body.getOrElse("category", Defaults.DEFAULT_CATEGORY).toString
    val userEmail = routeParams.param.getOrElse("email", "").toString
    followFeed(feedUrl, feedName, userEmail, userCategory, webClient, esWrapper)
  }

  def followFeed(feedUrl: String, feedName: String, userEmail: String, userCategory: String,
                 webClient: Option[WebClient], esWrapper: Option[EsClientWrapper]) : RouteResponse = {
    val uri = new URI(feedUrl)

    val feedRepository = new FeedRepository(esWrapper.get.indices(Indices.FEED))
    val existingFeed = feedRepository.get(esWrapper.get.esClient, feedUrl)
    var success : Boolean = true
    if (existingFeed.isEmpty) {
      success = createNewFeed(webClient, esWrapper, feedUrl, feedName, uri, userEmail)
      addFeedToTechUser(esWrapper.get, feedUrl, feedName)
    }
    val userFeedRepository = new UserFeedRepository(esWrapper.get.indices(Indices.USER_FEED))
    if (success && !userFeedRepository.exists(esWrapper.get.esClient, feedUrl, userEmail)) {
      userFeedRepository.index(esWrapper.get.esClient, UserFeed(feedName, feedUrl, userEmail, userCategory))
      if (existingFeed.nonEmpty) {
        copyFeedItemsToTheNewFollower(esWrapper.get, userEmail, feedUrl)
      }
      RouteResponse(204)
    } else {
      if (!success) {
        RouteResponse(500, Map(
          "error" -> "can't process feed %s".format(feedUrl),
          "code" -> 500
        ))
      } else {
        RouteResponse(304, Map(
          "error" -> "Feed already existing %s".format(feedUrl),
          "code" -> 304
        ))
      }
    }
  }

  private def addFeedToTechUser(esWrapper: EsClientWrapper, feedUrl: String, feedName: String) : Unit = {
    val userFeedRepository = new UserFeedRepository(esWrapper.indices(Indices.USER_FEED))
    userFeedRepository.index(esWrapper.esClient, UserFeed(feedName, feedUrl, getTechEmail))
  }

  private def copyFeedItemsToTheNewFollower(esWrapper: EsClientWrapper, userEmail: String, feedUrl: String) : Unit = {
    val userFeedItemRepository = new UserFeedItemRepository(esWrapper.indices(Indices.USER_FEED_ITEM))
    val feedItems = userFeedItemRepository.getAllFeedItemForUser(esWrapper.esClient, email = getTechEmail, Some(feedUrl), saved = false, 20)
    val userFeedItems = feedItems.map(feedItem => {
      feedItem.put(UserFeedItemField.EMAIL, userEmail)
      userFeedItemRepository.parse(feedItem)
    })
    userFeedItemRepository.insertAll(esWrapper.esClient, userFeedItems)
  }

  private def createNewFeed(webClient: Option[WebClient], esWrapper: Option[EsClientWrapper],
                            feedUrl: String, feedName: String, uri: URI, email: String) : Boolean = {
    try {
      val feed : SyndFeed = FeedParser.get(uri)
      val publicationDates : List[Option[Long]] = feed.getEntries.asScala.map(FeedParser.getPubDate).toList
      val maxPubDate = publicationDates.max
      val minPubDate = publicationDates.min
      val description = if(feed.getDescription != null) {feed.getDescription} else {""}
      val title = if(feed.getTitle != null) {feed.getTitle} else {""}
      val fullDescription = title + "\n" + description
      val language = if(feed.getLanguage != null) {feed.getLanguage} else {""}
      val feedRepository = new FeedRepository(esWrapper.get.indices(Indices.FEED))
      if (maxPubDate.isDefined && minPubDate.isDefined) {
        val publicationFrequency = assignFrequency(minPubDate.get, maxPubDate.get, publicationDates.size)
        val standByFrequency = isInStandby(publicationFrequency, maxPubDate.get)
        val faviconName = downloadFavicon(webClient, feed)
        feedRepository.create(esWrapper.get.esClient, feedUrl, feedName, language, fullDescription, publicationFrequency, standByFrequency, faviconName, email, true)
        true
      } else {
        false
      }
    } catch {
      case exception: Exception => false
    }
  }

  private def downloadFavicon(webClient: Option[WebClient], feed : SyndFeed) : String = {
    val entries = feed.getEntries
    if (entries.size() > 0) {
      val firstItem = entries.get(0)
      val url = firstItem.getLink
      val uri = new URI(url)
      val host = "icons.duckduckgo.com"
      val faviconFinder = "/ip3/"
      val suffix = ".ico"
      webClient.get.get(443, host, faviconFinder + uri.getHost + suffix)
        .ssl(true)
        .followRedirects(true)
        .send( ar => {
          val path = Paths.get(s"./favicons/${uri.getHost}$suffix")
          try {
            Files.write(path, ar.result().body().getBytes)
          } catch {
            case exception: Exception => false
          }
        })
      s"${uri.getHost}$suffix"
    } else {
      ""
    }
  }
}
