package io.telary.featherbrief.controller.utils

import java.time.temporal.TemporalUnit
import java.time.{Duration, LocalDateTime}

import io.telary.featherbrief.service.NodeClient
import io.telary.featherbrief.service.es.Converter
import io.vertx.core.MultiMap
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.{FileUpload, RoutingContext}
import io.vertx.ext.web.client.WebClient
import org.elasticsearch.client.RestHighLevelClient

import scala.collection.JavaConverters._

trait RouteHandler {
  /**
   * Parsed body and path params
   *
   * @param routingContext : the context
   * @return the params in a pretty object
   */
  private def parseBodyAndParams(routingContext: RoutingContext): RouteParams = {
    val body: JsonObject = routingContext.getBodyAsJson
    val bodyParamMap = if (body == null) { Map[String, Any]() } else { body.getMap.asScala.toMap }
    val pathParamMap = routingContext.pathParams.asScala.toMap
    val params : MultiMap = routingContext.request.params
    val paramMap : Map[String, String] = if (params.isEmpty) {
      Map[String, String]()
    } else {
      params.entries.asScala.map { entry =>
        (entry.getKey, entry.getValue)
      }.toMap
    }
    RouteParams(pathParamMap, bodyParamMap, paramMap)
  }

  final def getTechEmail : String = {
    "tech@telary.io"
  }

  protected def getEmail(routeParams: RouteParams): String = {
    routeParams.param.getOrElse("email", "").toString
  }

  protected def getUrl(routeParams: RouteParams): String = {
    routeParams.param.getOrElse("url", "").toString
  }

  protected def isSavedFilterPresent(routeParams: RouteParams): Boolean = {
    val filter = routeParams.param.getOrElse("filter", "").toString
    filter.toLowerCase.equals("SAVED".toLowerCase)
  }

  private def getUploadedFiles(routingContext: RoutingContext) : List[FileUpload] = {
    routingContext.fileUploads.asScala.toList
  }

  protected def getFeed(routeParams: RouteParams): Option[String] = {
    val feedUrl : String = routeParams.param.getOrElse("feed", "").toString
    if (feedUrl.isEmpty) {
      None
    } else {
      Some(feedUrl)
    }
  }

  def handle(routingContext: RoutingContext, webClient: WebClient = null, esClient: EsClientWrapper = null, nodeClient: NodeClient = null): Unit = {
    val startTime = LocalDateTime.now()
    val response = routingContext.response()
    try {
      val params = parseBodyAndParams(routingContext)
      val uploadedFiles = getUploadedFiles(routingContext)
      val routeResponse = process(params, Some(webClient), Some(esClient), Some(nodeClient), Some(uploadedFiles))
      response.putHeader("content-type", "application/json")
      //TODO avoid dysjonction in case of empty list
      val jsonString = if (routeResponse.list.nonEmpty && routeResponse.listName.nonEmpty) {
        Converter.toJsonObject(routeResponse.listName, routeResponse.list).encodePrettily()
      } else if (!routeResponse.json.isEmpty) {
        routeResponse.json
      } else {
        Converter.toJsonObject(routeResponse.content).encodePrettily()
      }

      val endTime = LocalDateTime.now()
      val duration = Duration.between(startTime, endTime)
      val logMessage = new LogMessage(
        routingContext.mountPoint() + routingContext.currentRoute().getPath,
        routingContext.currentRoute().methods.toString,
        endTime.toLocalDate.toString + endTime.toLocalTime.toString,
        routeResponse.status,
        duration.toMillis.toInt,
        ""
      )
      println(logMessage.toJson.toString)
      response.setStatusCode(routeResponse.status).end(jsonString)
    } catch {
      case e : Exception =>
        val endTime = LocalDateTime.now()
        val duration = Duration.between(startTime, endTime)
        val logMessage = new LogMessage(
          routingContext.mountPoint() + routingContext.currentRoute().getPath,
          routingContext.currentRoute().methods.toString,
          endTime.toLocalDate.toString + endTime.toLocalTime.toString,
          500,
          duration.toMillis.toInt,
          e.getMessage
        )
        println(logMessage.toJson.toString)
        e.printStackTrace()
        response.setStatusCode(500).end("{}")
    }
  }

  /**
   * handle the route processing
   *
   * @param routeParams : path and body params
   * @return the response
   */
   def process(routeParams: RouteParams,
                webClient: Option[WebClient],
                esWrapper: Option[EsClientWrapper],
                nodeClient: Option[NodeClient],
                uploadedFiles: Option[List[FileUpload]]
               ): RouteResponse
}
