package io.telary.featherbrief.controller.utils

import io.vertx.lang.scala.json.JsonObject

import scala.collection.mutable

case class RouteResponse(status : Int,
                         content : Map[String, Any] = Map(),
                         list : List[mutable.Map[String, AnyRef]] = List(),
                         listName : String = "",
                         json : String = ""
                        )
