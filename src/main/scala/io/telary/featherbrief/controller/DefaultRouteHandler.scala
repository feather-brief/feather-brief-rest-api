package io.telary.featherbrief.controller

import io.telary.featherbrief.model.objects.Feed
import io.telary.featherbrief.service.es.Converter
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import org.elasticsearch.client.core.MainResponse
import org.elasticsearch.client.{RequestOptions, RestHighLevelClient}

trait DefaultRouteHandler {
  def defaultRouteHandler(routingContext: RoutingContext, esClient: RestHighLevelClient): Unit = {
    // This handler will be called for every request
    val response = routingContext.response()
    response.putHeader("content-type", "application/json")
    val esResponse : MainResponse = esClient.info(RequestOptions.DEFAULT)

    val statusMap = new JsonObject
    statusMap.put("Name", esResponse.getClusterName)
    statusMap.put("Version", esResponse.getVersion.getNumber)

    // Write to the response and end it
    val json : JsonObject = new JsonObject
    json.put("AppName", "Vert.x Scala server")
    json.put("Status", "UP")
    json.put("Date", java.time.LocalDate.now.toString)
    json.put("EsStatus", statusMap)

    response.end(json.encodePrettily())
  }
}
