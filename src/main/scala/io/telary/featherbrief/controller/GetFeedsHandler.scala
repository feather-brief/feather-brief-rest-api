package io.telary.featherbrief.controller

import io.telary.featherbrief.controller.utils.{EsClientWrapper, RouteHandler, RouteParams, RouteResponse}
import io.telary.featherbrief.model.objects.Indices
import io.telary.featherbrief.model.repositories.{FeedRepository, UserFeedRepository}
import io.telary.featherbrief.service.NodeClient
import io.telary.featherbrief.service.es.Converter
import io.vertx.ext.web.FileUpload
import io.vertx.ext.web.client.WebClient
import org.elasticsearch.client.RestHighLevelClient

object GetFeedsHandler extends RouteHandler {
  /**
   * handle the route processing
   *
   * @param routeParams : path and body params
   * @return the response
   */
  override def process(routeParams: RouteParams,
                                 webClient: Option[WebClient],
                                 esWrapper: Option[EsClientWrapper],
                                 nodeClient: Option[NodeClient],
                                 uploadedFiles: Option[List[FileUpload]]): RouteResponse = {
    val email = getEmail(routeParams)
    val userFeedRepository = new UserFeedRepository(esWrapper.get.indices(Indices.USER_FEED))
    val feedsFound = userFeedRepository.getAll(esWrapper.get.esClient, email)
    RouteResponse(200, list = feedsFound, listName = "feeds")
  }
}
