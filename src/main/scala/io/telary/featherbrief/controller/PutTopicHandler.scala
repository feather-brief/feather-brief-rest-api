package io.telary.featherbrief.controller

import io.telary.featherbrief.controller.utils.{EsClientWrapper, RouteHandler, RouteParams, RouteResponse}
import io.telary.featherbrief.model.objects.{Indices, UserFeedItem}
import io.telary.featherbrief.model.repositories.{TopicRepository, UserFeedItemRepository}
import io.telary.featherbrief.service.NodeClient
import io.telary.featherbrief.service.es.Converter
import io.vertx.ext.web.FileUpload
import io.vertx.ext.web.client.WebClient
import org.elasticsearch.client.RestHighLevelClient

object PutTopicHandler extends RouteHandler {
  /**
   * handle the route processing
   *
   * @param routeParams : path and body params
   * @return the response
   */
  override def process(routeParams: RouteParams,
                                 webClient: Option[WebClient],
                                 esWrapper: Option[EsClientWrapper],
                                 nodeClient: Option[NodeClient],
                                 uploadedFiles: Option[List[FileUpload]]): RouteResponse = {
    val containsName = routeParams.param.contains("name")
    val containsUrl = routeParams.param.contains("url")
    val userFeedItemRepository = new UserFeedItemRepository(esWrapper.get.indices(Indices.USER_FEED_ITEM))
    if (containsName && containsUrl) {
      val url = routeParams.param("url").toString
      val name = routeParams.param("name").toString
      val feedItem = userFeedItemRepository.getFeedItemForUser(esWrapper.get.esClient, getEmail(routeParams), url)
      if (feedItem.articleUrl.isEmpty) {
        RouteResponse(404, content = Map("error" -> "Article not found"))
      } else {
        val userFeedItemToSave = UserFeedItem(
          feedItem.email,
          name,
          s"/secured/topic?name=$name",
          feedItem.articleUrl,
          feedItem.author,
          feedItem.description,
          feedItem.date,
          feedItem.summary,
          feedItem.title,
          feedItem.score,
          feedItem.shown,
          feedItem.read,
          feedItem.saved,
          feedItem.upvote,
          feedItem.downvote,
          feedItem.favicon,
          feedItem.parsable,
          feedItem.category
        )
        TopicRepository.index(esWrapper.get.esClient, userFeedItemToSave)
        RouteResponse(200, json = Converter.toJsonObject(userFeedItemToSave).encodePrettily())
      }
    } else {
      RouteResponse(406, content = Map("error" -> "Invalid request"))
    }
  }
}
