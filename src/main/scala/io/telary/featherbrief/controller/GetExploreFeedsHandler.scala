package io.telary.featherbrief.controller

import io.telary.featherbrief.controller.utils.{EsClientWrapper, RouteHandler, RouteParams, RouteResponse}
import io.telary.featherbrief.model.objects.Indices
import io.telary.featherbrief.model.repositories.FeedRepository
import io.telary.featherbrief.service.NodeClient
import io.vertx.ext.web.FileUpload
import io.vertx.ext.web.client.WebClient
import org.elasticsearch.client.RestHighLevelClient

object GetExploreFeedsHandler extends RouteHandler {
  /**
   * handle the route processing
   *
   * @param routeParams : path and body params
   * @return the response
   */
  override def process(routeParams: RouteParams,
                                 webClient: Option[WebClient],
                                 esWrapper: Option[EsClientWrapper],
                                 nodeClient: Option[NodeClient],
                                 uploadedFiles: Option[List[FileUpload]]): RouteResponse = {
    val feedRepository = new FeedRepository(esWrapper.get.indices(Indices.FEED))
    val feeds = feedRepository.getAll(esWrapper.get.esClient)
    RouteResponse(200, listName = "feeds", list = feeds)
  }
}
