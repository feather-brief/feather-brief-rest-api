package io.telary.featherbrief.controller

import io.telary.featherbrief.controller.utils.{EsClientWrapper, RouteHandler, RouteParams, RouteResponse}
import io.telary.featherbrief.model.objects.Indices
import io.telary.featherbrief.model.repositories.BundleRepository
import io.telary.featherbrief.service.NodeClient
import io.telary.featherbrief.service.es.Converter
import io.vertx.ext.web.FileUpload
import io.vertx.ext.web.client.WebClient

object GetBundlesHandler extends RouteHandler {
  /**
   * handle the route processing
   *
   * @param routeParams : path and body params
   * @return the response
   */
  override def process(routeParams: RouteParams, webClient: Option[WebClient], esWrapper: Option[EsClientWrapper],
                       nodeClient: Option[NodeClient], uploadedFiles: Option[List[FileUpload]]): RouteResponse = {
    val bundleRepository = new BundleRepository(esWrapper.get.indices(Indices.BUNDLE))
    val bundles = bundleRepository.getAll(esWrapper.get.esClient)
    RouteResponse(200, json = Converter.toJsonArray(bundles).encodePrettily())
  }
}
