package io.telary.featherbrief.controller

import java.io.File
import io.telary.featherbrief.controller.utils.{EsClientWrapper, RouteHandler, RouteParams, RouteResponse}
import io.telary.featherbrief.model.Defaults
import io.telary.featherbrief.service.{NodeClient, OpmlParser}
import io.vertx.ext.web.FileUpload
import io.vertx.ext.web.client.WebClient
import org.elasticsearch.client.RestHighLevelClient

object PutOpmlHandler extends RouteHandler {
  /**
   * handle the route processing
   *
   * @param routeParams : path and body params
   * @return the response
   */
  override def process(routeParams: RouteParams, webClient: Option[WebClient],
                                 esWrapper: Option[EsClientWrapper], nodeClient: Option[NodeClient],
                                 uploadedFiles: Option[List[FileUpload]])
  : RouteResponse = {
    if (uploadedFiles.get.isEmpty) {
      RouteResponse(500)
    } else {
      //curl -i --request PUT --url http://localhost:8080/secured/opml -F'opml=@/home/XXX/opml.xml'
      val uploads = uploadedFiles.get
      val filename = uploads.head.uploadedFileName()
      val feedsToAdd = OpmlParser.parse(filename, getEmail(routeParams))
      val thread = new Thread {
        override def run() {
          new File(filename).delete()
          feedsToAdd.foreach(feed => {
            PostFeedHandler.followFeed(feed.url, feed.name, getEmail(routeParams), Defaults.DEFAULT_CATEGORY, webClient, esWrapper)
            Thread.sleep(100)
          })
        }
      }
      thread.start()
      RouteResponse(200)
    }
  }
}
