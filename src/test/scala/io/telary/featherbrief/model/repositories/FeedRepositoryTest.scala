package io.telary.featherbrief.model.repositories

import io.telary.featherbrief.model.objects.Indices
import io.telary.featherbrief.service.ConfigFile
import org.apache.http.HttpHost
import org.apache.http.auth.{AuthScope, UsernamePasswordCredentials}
import org.apache.http.impl.client.BasicCredentialsProvider
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder
import org.elasticsearch.client.{RestClient, RestHighLevelClient}
import org.scalatest._
import org.scalatest.matchers.should.Matchers

class FeedRepositoryTest extends FlatSpec
  with Matchers with RepositorySuite {

  "Feed actions" should "successfully update ES database" in {
    val esClient = RepositoryUtil.esClient()
    val feedRepository = new FeedRepository(Indices.FEED_TEST)
    val feedUrl = "http://testurl.com"
    feedRepository.create(esClient, feedUrl, "Test Feed", "", "",
      1, None, "", "", parsable = true)
    Thread.sleep(1000)
    feedRepository.update(esClient, feedUrl, 2, 10000L)
    Thread.sleep(1000)
    val feed = feedRepository.get(esClient, feedUrl)
    feed.isDefined shouldBe(true)
    feed.get.lastPublicationDate shouldBe(10000L)
    feed.get.publicationFrequency shouldBe(2)
    val feedList = feedRepository.getForFrequency(esClient, 1)
    feedList.size shouldBe(0)
    val feedList2 = feedRepository.getForFrequency(esClient, 2)
    feedList2.head.url shouldBe feedUrl
    val feedList3 = feedRepository.getAll(esClient)
    feedList3.head("url") shouldBe feedUrl
  }
}
