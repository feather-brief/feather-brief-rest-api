package io.telary.featherbrief.model.repositories

import java.util.Calendar

import io.telary.featherbrief.model.objects.Indices
import io.telary.featherbrief.service.ConfigFile
import org.apache.http.HttpHost
import org.apache.http.auth.{AuthScope, UsernamePasswordCredentials}
import org.apache.http.impl.client.BasicCredentialsProvider
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder
import org.elasticsearch.ElasticsearchStatusException
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.client.{RequestOptions, RestClient, RestHighLevelClient}
import org.scalatest.{BeforeAndAfterAll, Suite}

trait RepositorySuite extends Suite with BeforeAndAfterAll{
  override def afterAll(): Unit = {
    println("After all")
    val esClient = RepositoryUtil.esClient()
    val deleteFeed = new DeleteIndexRequest(Indices.FEED_TEST)
    val deleteUserFeed = new DeleteIndexRequest(Indices.USER_FEED_TEST)
    val deleteUserFeedItem = new DeleteIndexRequest(Indices.USER_FEED_ITEM_TEST)
    val accesslog = new DeleteIndexRequest(Indices.ACCESS_LOG_TEST)
    val bundle = new DeleteIndexRequest(Indices.BUNDLE_TEST)
    try {
      esClient.indices.delete(deleteFeed, RequestOptions.DEFAULT)
    } catch {
      case e : ElasticsearchStatusException => None
    }
    try {
      esClient.indices.delete(deleteUserFeed, RequestOptions.DEFAULT)
    } catch {
      case e : ElasticsearchStatusException => None
    }
    try {
      esClient.indices.delete(deleteUserFeedItem, RequestOptions.DEFAULT)
    } catch {
      case e : ElasticsearchStatusException => None
    }
    try {
      esClient.indices.delete(bundle, RequestOptions.DEFAULT)
    } catch {
      case e : ElasticsearchStatusException => None
    }
    try {
      esClient.indices.delete(accesslog, RequestOptions.DEFAULT)
    } catch {
      case e : ElasticsearchStatusException => None
    }
    try {
      val day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
      val month = Calendar.getInstance().get(Calendar.MONTH)
      val year = Calendar.getInstance().get(Calendar.YEAR)
      val index = Indices.ARTICLE_TEST + "-" + year + "-" + month + "-" + day
      val deleteArticle = new DeleteIndexRequest(index)
      esClient.indices.delete(deleteArticle, RequestOptions.DEFAULT)
    } catch {
      case e : ElasticsearchStatusException => None
    }
  }
}

object RepositoryUtil {
  def esClient() : RestHighLevelClient = {
    val config = ConfigFile.parse("src/test/resources/REPOSITORY_TESTS_CONF")

    val credentialsProvider = new BasicCredentialsProvider
    credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(config.esUsername, config.esPassword))

    val builder = RestClient.builder(new HttpHost(config.esHost, config.esPort.toInt))
      .setHttpClientConfigCallback((httpClientBuilder: HttpAsyncClientBuilder) => {
        httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider)
      })

    new RestHighLevelClient(builder)
  }
}
