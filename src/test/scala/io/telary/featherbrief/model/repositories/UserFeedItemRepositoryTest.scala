package io.telary.featherbrief.model.repositories

import io.telary.featherbrief.model.objects.{Indices, UserFeedItem, UserFeedItemField}
import org.scalatest._
import org.scalatest.matchers.should.Matchers

class UserFeedItemRepositoryTest  extends FlatSpec
  with Matchers with RepositorySuite {

  "User feed items request" should "successfully update ES database" in {
    val userFeedItemRepository = new UserFeedItemRepository(Indices.USER_FEED_ITEM_TEST)
    val esClient = RepositoryUtil.esClient()
    val email = "test@email.com"
    val ufiList = List(
      new UserFeedItem(email, "feed1", "feed-url", "1", "", "", 1L,
        "", "", 1, false, false, false, false,
        false, "", true),
      new UserFeedItem(email, "feed1", "feed-url", "2", "", "", 1L,
        "", "", 2, false, false, false, false,
        false, "", true),
      new UserFeedItem(email, "feed2", "feed-url", "3", "", "", 1L,
        "", "", 3, false, false, false, false,
        false, "", true),
      new UserFeedItem(email, "feed2", "feed-url", "4", "", "", 1L,
        "", "", 4, false, false, true, false,
        false, "", true)
    )
    userFeedItemRepository.insertAll(esClient, ufiList)
    Thread.sleep(1000)
    val unsavedUfiForUser = userFeedItemRepository.getAllFeedItemForUser(esClient, email, None, saved = false, 20)
    val savedUfiForUser = userFeedItemRepository.getAllFeedItemForUser(esClient, email, None, saved = true, 20)
    val specificFeedItem = userFeedItemRepository.getFeedItemForUser(esClient, email, "1")
    unsavedUfiForUser.size shouldBe(3)
    savedUfiForUser.size shouldBe(1)
    specificFeedItem.articleUrl shouldBe("1")
  }

  "Changing category of all feed items" should "successfully update ES database" in {
    val userFeedItemRepository = new UserFeedItemRepository(Indices.USER_FEED_ITEM_TEST)
    val esClient = RepositoryUtil.esClient()
    val email = "test2@email.com"
    val ufiList = List(
      new UserFeedItem(email, "feed1", "feed-url", "1", "", "", 1L,
        "", "", 1, false, false, false, false,
        false, "", true, "1"),
      new UserFeedItem(email, "feed1", "feed-url", "2", "", "", 1L,
        "", "", 2, false, false, false, false,
        false, "", true, "1"),
      new UserFeedItem(email, "feed1", "feed-url", "3", "", "", 1L,
        "", "", 3, false, false, false, false,
        false, "", true, "1"),
      new UserFeedItem(email, "feed2", "feed-url-2", "4", "", "", 1L,
        "", "", 4, false, false, false, false,
        false, "", true, "2")
    )
    userFeedItemRepository.insertAll(esClient, ufiList)
    Thread.sleep(1000)
    userFeedItemRepository.updateFeedItemsCategory(esClient, email, "feed-url", "1", "newcat")
    Thread.sleep(1000)
    val updatedFeedItems = userFeedItemRepository.getAllFeedItemForUser(esClient, email, None, false, 5)
    updatedFeedItems.size shouldBe(4)
    var i = 0
    for(feeditem <- updatedFeedItems) {
      if (feeditem(UserFeedItemField.FEED_NAME) == "feed1") {
        feeditem(UserFeedItemField.CATEGORY) shouldBe "newcat"
        i += 1
      } else {
        feeditem(UserFeedItemField.CATEGORY) shouldBe "2"
      }
    }
    i shouldBe 3
  }
}
