package io.telary.featherbrief.controller

import io.telary.featherbrief.controller.utils.{EsClientWrapper, RouteParams}
import io.telary.featherbrief.model.Defaults
import io.telary.featherbrief.model.objects.{Indices, UserFeed, UserFeedItem}
import io.telary.featherbrief.model.repositories.{RepositorySuite, RepositoryUtil, UserFeedItemRepository, UserFeedRepository}
import org.scalatest._
import org.scalatest.matchers.should.Matchers

class PutFeedHandlerTest extends FlatSpec
  with Matchers with RepositorySuite {
  "[PUT] feed" should "let a user update a feed he already follows" in {
    val esClient = RepositoryUtil.esClient()
    val esWrapper = Some(EsClientWrapper(esClient,
      Map[String, String](
        Indices.USER_FEED -> Indices.USER_FEED_TEST,
        Indices.USER_FEED_ITEM -> Indices.USER_FEED_ITEM_TEST
      )))
    val email = "test@email.com"
    val routeParams = RouteParams(
      null,
      null,
      Map[String, String](
        "email" -> email,
        "url" -> "1",
        "name" -> "1",
        "new_category" -> "updated",
        "old_category" -> "not_updated"
      )
    )
    val userFeed = UserFeed(
      "1",
      "1",
      email,
      "not_updated"
    )
    val userFeedItem = UserFeedItem(email, "1", "1", "1", "", "", 1L,
      "", "", 4, false, false, false, false,
      false, "", true, "not_updated")
    val userFeedRepository = new UserFeedRepository(Indices.USER_FEED_TEST)
    val userFeedItemRepository = new UserFeedItemRepository(Indices.USER_FEED_ITEM_TEST)
    val created = userFeedRepository.index(esClient, userFeed)
    userFeedItemRepository.insertAll(esClient, List(userFeedItem))
    Thread.sleep(1000)
    PutFeedHandler.process(routeParams, null, esWrapper, null, null)
    Thread.sleep(1000)
    val followedFeeds = userFeedRepository.getAll(esClient, email)
    followedFeeds.head("category") shouldBe "updated"
  }
}
