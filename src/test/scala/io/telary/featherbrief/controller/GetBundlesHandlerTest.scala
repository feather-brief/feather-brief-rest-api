package io.telary.featherbrief.controller

import io.telary.featherbrief.controller.utils.EsClientWrapper
import io.telary.featherbrief.model.objects.{Bundle, BundleField, Feed, Indices}
import io.telary.featherbrief.model.repositories.{BundleRepository, RepositorySuite, RepositoryUtil}
import io.vertx.core.json.JsonArray
import org.scalatest._
import org.scalatest.matchers.should.Matchers

class GetBundlesHandlerTest extends FlatSpec
  with Matchers with RepositorySuite {

  "[GET] bundles" should "return some bundles" in {
    val esClient = RepositoryUtil.esClient()
    val esWrapper = Some(EsClientWrapper(esClient,
      Map[String, String](
        Indices.BUNDLE -> Indices.BUNDLE_TEST
      )))

    val bundleRepository = new BundleRepository(Indices.BUNDLE_TEST)

    val bundleName1 = "Test_bundle_1"
    val bundleName2 = "Test_bundle_2"

    val bundle1 = new Bundle(
      bundleName1,
      "fr",
      List(
        Feed("test #1", "", 0, 0, isInStandby = false, "fr", "", "", "", parsable = true),
        Feed("test #2", "", 0, 0, isInStandby = false, "fr", "", "", "", parsable = true)
      )
    )

    val bundle2 = new Bundle(
      bundleName2,
      "fr",
      List(
        Feed("test #1", "", 0, 0, isInStandby = false, "fr", "", "", "", parsable = true),
        Feed("test #2", "", 0, 0, isInStandby = false, "fr", "", "", "", parsable = true),
        Feed("test #3", "", 0, 0, isInStandby = false, "fr", "", "", "", parsable = true),
        Feed("test #4", "", 0, 0, isInStandby = false, "fr", "", "", "", parsable = true)
      )
    )

    bundleRepository.insert(esClient, bundle1)
    bundleRepository.insert(esClient, bundle2)
    Thread.sleep(1000)
    val response = GetBundlesHandler.process(null, null, esWrapper, null, null)
    val json = new JsonArray(response.json)
    json.size shouldBe 2
    json.getJsonObject(0).getString(BundleField.NAME) shouldBe bundleName1
    json.getJsonObject(1).getString(BundleField.NAME) shouldBe bundleName2
  }
}
