package io.telary.featherbrief.controller

import io.telary.featherbrief.controller.utils.{EsClientWrapper, RouteParams}
import io.telary.featherbrief.model.objects.Indices
import io.telary.featherbrief.model.repositories.{RepositorySuite, RepositoryUtil, UserFeedItemRepository, UserFeedRepository}
import io.vertx.core.{Vertx, VertxOptions}
import io.vertx.ext.web.client.{WebClient, WebClientOptions}
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.indices.CreateIndexRequest
import org.scalatest._
import org.scalatest.matchers.should.Matchers

class PostFeedHandlerTest extends FlatSpec
  with Matchers with RepositorySuite {
  "[POST] feed" should "let a user follow a feed" in {
    val esClient = RepositoryUtil.esClient()
    val esWrapper = Some(EsClientWrapper(esClient,
      Map[String, String](
        Indices.USER_FEED -> Indices.USER_FEED_TEST,
        Indices.FEED -> Indices.FEED_TEST,
        Indices.USER_FEED_ITEM -> Indices.USER_FEED_ITEM_TEST
      )))
    val feedUrl = "https://feather-brief.gitlab.io/feather-brief-testing-resources/site-1.xml"
    val routeParams = RouteParams(
      null,
      Map[String, String](
        "url" -> feedUrl,
        "name" -> "Feed #1"
      ),
      Map[String, String](
        "email" -> "test@email.com"
      )
    )
    val vOptions = new VertxOptions
    vOptions.setBlockedThreadCheckInterval(1000 * 60 * 60)
    val vertx = Vertx.vertx(vOptions)
    val options = new WebClientOptions
    options.setUserAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0")
    options.setKeepAlive(false)
    val httpClient = WebClient.create(vertx, options)

    val createFeedTest = new CreateIndexRequest(Indices.FEED_TEST)
    val createUserFeedTest = new CreateIndexRequest(Indices.USER_FEED_TEST)
    val createUserFeedItemTest = new CreateIndexRequest(Indices.USER_FEED_ITEM_TEST)
    esClient.indices.create(createFeedTest, RequestOptions.DEFAULT)
    esClient.indices.create(createUserFeedTest, RequestOptions.DEFAULT)
    esClient.indices.create(createUserFeedItemTest, RequestOptions.DEFAULT)
    Thread.sleep(1000)
    val response = PostFeedHandler.process(routeParams, Some(httpClient), esWrapper, null, null)
    response.status shouldBe 204
    Thread.sleep(1000)
    val userFeedRepository = new UserFeedRepository(Indices.USER_FEED_TEST)
    val userFeeds = userFeedRepository.getAllUserFollowing(esClient, feedUrl)
    userFeeds.size shouldBe 2
  }
}
