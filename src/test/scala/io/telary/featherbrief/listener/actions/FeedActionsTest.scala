package io.telary.featherbrief.listener.actions

import java.util.Calendar
import java.util.Calendar._

import org.scalatest._
import org.scalatest.matchers.should.Matchers

class FeedActionsTest extends FlatSpec
  with Matchers with FeedFrequencies with FeedActions {

  "Determine frequency" should "give me WEEKLY" in {
    determineFrequencyForRun(createDate(12, APRIL, 2020, 1, 0)) shouldBe WEEKLY
  }

  "Determine frequency" should "give me DAILY" in {
    determineFrequencyForRun(createDate(13, APRIL, 2020, 1, 30)) shouldBe DAILY
    determineFrequencyForRun(createDate(14, APRIL, 2020, 1, 30)) shouldBe DAILY
  }

  "Determine frequency" should "give me HOURLY" in {
    determineFrequencyForRun(createDate(13, APRIL, 2020, 0, 0)) shouldBe HOURLY
    determineFrequencyForRun(createDate(13, APRIL, 2020, 3, 0)) shouldBe HOURLY
    determineFrequencyForRun(createDate(13, APRIL, 2020, 10, 0)) shouldBe HOURLY
    determineFrequencyForRun(createDate(13, APRIL, 2020, 12, 0)) shouldBe HOURLY
  }

  "Determine frequency" should "give me HALF_HOURLY" in {
    determineFrequencyForRun(createDate(13, APRIL, 2020, 4, 40)) shouldBe HALF_HOURLY
    determineFrequencyForRun(createDate(13, APRIL, 2020, 4, 10)) shouldBe HALF_HOURLY
  }

  "Determine frequency" should "give me TENTH_MINUTE" in {
    determineFrequencyForRun(createDate(13, APRIL, 2020, 4, 20)) shouldBe TENTH_MINUTE
    determineFrequencyForRun(createDate(13, APRIL, 2020, 4, 30)) shouldBe TENTH_MINUTE
    determineFrequencyForRun(createDate(13, APRIL, 2020, 4, 50)) shouldBe TENTH_MINUTE
  }

  "Determine frequency" should "give me EVERY_MINUTE" in {
    determineFrequencyForRun(createDate(13, APRIL, 2020, 0, 1)) shouldBe EVERY_MINUTE
    determineFrequencyForRun(createDate(13, APRIL, 2020, 4, 45)) shouldBe EVERY_MINUTE
    determineFrequencyForRun(createDate(13, APRIL, 2020, 11, 23)) shouldBe EVERY_MINUTE
  }

  val feedQuantity = 10
  "Determine publication frequency" should "give me WEEKLY" in {
    val min = createDate(30, DECEMBER, 2019, 11, 0).toInstant.toEpochMilli
    val max = createDate(2, MARCH, 2020, 11, 0).toInstant.toEpochMilli
    assignFrequency(min, max, feedQuantity) shouldBe WEEKLY
    val min2 = createDate(30, DECEMBER, 2019, 11, 0).toInstant.toEpochMilli
    val max2 = createDate(2, DECEMBER, 2020, 11, 0).toInstant.toEpochMilli
    assignFrequency(min2, max2, feedQuantity) shouldBe WEEKLY
  }

  "Determine publication frequency" should "give me DAILY" in {
    val min = createDate(1, JANUARY, 2020, 11, 0).toInstant.toEpochMilli
    val max = createDate(1, FEBRUARY, 2020, 11, 0).toInstant.toEpochMilli
    assignFrequency(min, max, feedQuantity) shouldBe DAILY
    assignFrequency(max, max, feedQuantity) shouldBe DAILY
  }

  "Determine publication frequency" should "give me HOURLY" in {
    val min = createDate(1, JANUARY, 2020, 6, 0).toInstant.toEpochMilli
    val max = createDate(1, JANUARY, 2020, 16, 0).toInstant.toEpochMilli
    assignFrequency(min, max, feedQuantity) shouldBe HOURLY
    val min2 = createDate(1, JANUARY, 2020, 11, 0).toInstant.toEpochMilli
    val max2 = createDate(5, JANUARY, 2020, 11, 0).toInstant.toEpochMilli
    assignFrequency(min2, max2, feedQuantity) shouldBe HOURLY
    val min3 = createDate(1, JANUARY, 2020, 11, 0).toInstant.toEpochMilli
    val max3 = createDate(10, JANUARY, 2020, 11, 0).toInstant.toEpochMilli
    assignFrequency(min3, max3, feedQuantity) shouldBe HOURLY
  }

  "Determine publication frequency" should "give me HALF_HOURLY" in {
    val min = createDate(1, JANUARY, 2020, 6, 0).toInstant.toEpochMilli
    val max = createDate(1, JANUARY, 2020, 11, 0).toInstant.toEpochMilli
    assignFrequency(min, max, feedQuantity) shouldBe HALF_HOURLY
  }

  "Determine publication frequency" should "give me TENTH_MINUTE" in {
    val min = createDate(1, JANUARY, 2020, 6, 0).toInstant.toEpochMilli
    val max = createDate(1, JANUARY, 2020, 8, 0).toInstant.toEpochMilli
    assignFrequency(min, max, feedQuantity) shouldBe TENTH_MINUTE
  }

  "Determine publication frequency" should "give me EVERY_MINUTE" in {
    val min = createDate(1, JANUARY, 2020, 6, 0).toInstant.toEpochMilli
    val max = createDate(1, JANUARY, 2020, 6, 20).toInstant.toEpochMilli
    assignFrequency(min, max, feedQuantity) shouldBe EVERY_MINUTE
    val min2 = createDate(1, JANUARY, 2020, 6, 0).toInstant.toEpochMilli
    val max2 = createDate(1, JANUARY, 2020, 7, 25).toInstant.toEpochMilli
    assignFrequency(min2, max2, feedQuantity) shouldBe EVERY_MINUTE
  }

  def createDate(day: Int, month: Int, year: Int, hour: Int, minute: Int) : Calendar = {
    val date = Calendar.getInstance
    date.set(Calendar.DAY_OF_MONTH, day)
    date.set(Calendar.MONTH, month)
    date.set(Calendar.YEAR, year)
    date.set(Calendar.HOUR_OF_DAY, hour)
    date.set(Calendar.MINUTE, minute)
    date
  }
}
