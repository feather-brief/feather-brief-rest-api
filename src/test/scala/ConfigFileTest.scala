import io.telary.featherbrief.service.ConfigFile
import org.scalatest._
import org.scalatest.matchers.should.Matchers

class ConfigFileTest extends FlatSpec with Matchers{
  "ConfigFile" should "have an ES_PORT and ES_HOST key" in {
    val params = ConfigFile.parse("src/test/resources/TEST_CONFIG")
    params.esHost shouldEqual("FAKE_HOST")
    params.esPort shouldEqual("FAKE_PORT")
    params.esPassword shouldEqual("")
  }
}
